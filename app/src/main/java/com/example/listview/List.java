package com.example.listview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class List extends ArrayAdapter<ItemData> {
    int groupId;
    Activity Context;
    ArrayList<ItemData> list;
    ArrayList<ItemData> originalList; // Copia de respaldo de la lista original
    ArrayList<ItemData> filteredList; // Lista filtrada

    LayoutInflater inflater;

    public List(Activity Context, int groupId, int id, ArrayList<ItemData> list) {
        super(Context, id, list);
        this.originalList = new ArrayList<>(list);
        this.filteredList = new ArrayList<>(list);
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    // Restaurar la lista original
    public void resetData() {
        filteredList = new ArrayList<>(originalList);
    }
    // Obtener el número de elementos en la lista filtrada
    @Override
    public int getCount() {
        return filteredList.size();
    }

    // Obtener el elemento en una posición de la lista filtrada
    @Override
    public ItemData getItem(int position) {
        return filteredList.get(position);
    }
    // Filtrar la lista según el texto de búsqueda
    public void filter(String query) {
        filteredList.clear();
        if (query.isEmpty()) {
            filteredList.addAll(originalList);
        } else {
            query = query.toLowerCase();
            for (ItemData item : originalList) {
                if (item.getMatricula().toLowerCase().contains(query) ||
                        item.getNombreAlumno().toLowerCase().contains(query)) {
                    filteredList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(groupId, parent, false);
        }

        ImageView imageView = itemView.findViewById(R.id.imgCategoria);
        TextView txtMatricula = itemView.findViewById(R.id.lblCategorias);
        TextView txtNombre = itemView.findViewById(R.id.lblDescripcion);

        ItemData currentItem = filteredList.get(position);

        imageView.setImageResource(currentItem.getImgAlumno());
        txtMatricula.setText(currentItem.getMatricula());
        txtNombre.setText(currentItem.getNombreAlumno());

        return itemView;
    }








    /*
    public List(Activity Context,int groupId,int id,ArrayList<ItemData>
            list){
        super(Context,id,list);
        this.list = list;
        inflater = (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;}

    public View getView(int posicion, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupId,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgCategoria);
        imagen.setImageResource(list.get(posicion).getImgAlumno());
        TextView textCategoria = (TextView) itemView.findViewById(R.id.lblCategorias);
        textCategoria.setText(list.get(posicion).getNombreAlumno());
        TextView textDescripcion = (TextView) itemView.findViewById(R.id.lblDescripcion);
        textDescripcion.setText(list.get(posicion).getMatricula());
        return itemView;
    }*/
    public View getDropDownView(int posicion,View convertView,ViewGroup parent){
        return getView(posicion,convertView,parent);
    }
}