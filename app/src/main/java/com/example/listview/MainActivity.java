package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.SearchView;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    List adapter;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<ItemData> list = new ArrayList<>();

        list.add(new ItemData(R.drawable.a2019030344,"MORUA ZAMUDIO ESTEFANO", "2019030344"));
        list.add(new ItemData(R.drawable.a2020030174,"CARRANZA JAUREGUI CARLOS ALBERTO", "2020030174"));
        list.add(new ItemData(R.drawable.a2020030176,"MORUA ZAMUDIO ESTEFANO", "2019030344"));
        list.add(new ItemData(R.drawable.a2020030181,"DURAN VALDEZ JOSHUA DANIEL", "2020030181"));
        list.add(new ItemData(R.drawable.a2020030184,"GALINDO HERNANDEZ ERNESTO DAVID", "2020030184"));
        list.add(new ItemData(R.drawable.a2020030189,"CONTRERAS CEPEDA MAXIMILIANO\n", "2020030189"));
        list.add(new ItemData(R.drawable.a2020030199,"GOMEZ RUELAS IVÁN ENRIQUE\n", "2020030199"));
        list.add(new ItemData(R.drawable.a2020030212,"CRUZ QUINTERO JESUS EDUARDO\n", "2020030212"));
        list.add(new ItemData(R.drawable.a2020030241,"VELARDE OVALLE DAVID ANTONIO\n", "2020030241"));
        list.add(new ItemData(R.drawable.a2020030243,"LAMAS ARMENTA GUSTAVO ADOLFO\n", "2020030243"));
        list.add(new ItemData(R.drawable.a2020030249,"RIVAS LUGO JUAN CARLOS\n", "2020030249"));
        list.add(new ItemData(R.drawable.a2020030264,"SALAS MENDOZA ALEJO\n", "2020030264"));
        list.add(new ItemData(R.drawable.a2020030268,"SERRANO TORRES CARLOS JAIR\n", "2020030268"));
        list.add(new ItemData(R.drawable.a2020030292,"TIRADO ROMERO JESUS TADEO\n", "2020030292"));
        list.add(new ItemData(R.drawable.a2020030304,"CARRILLO GARCIA JAIR\n", "2020030304"));
        list.add(new ItemData(R.drawable.a2020030306,"ARIAS ZATARAIN DIEGO\n", "2020030306"));
        list.add(new ItemData(R.drawable.a2020030313,"VALDEZ MARTINEZ PAOLA EMIRET\n", "2020030313"));
        list.add(new ItemData(R.drawable.a2020030315,"IBARRA FLORES SALMA YARETH\n", "2020030315"));
        list.add(new ItemData(R.drawable.a2020030322,"LIZARRAGA MALDONADO JUAN ANTONIO\n", "2020030322"));
        list.add(new ItemData(R.drawable.a2020030325,"VIERA ROMERO ANGEL ZINEDINE ANASTACIO\n", "2020030325"));
        list.add(new ItemData(R.drawable.a2020030327,"TEJEDA PEINADO BLAS ALBERTO\n", "2020030327"));
        list.add(new ItemData(R.drawable.a2020030329,"VIERA ROMERO ANGEL RONALDO ANASTACIO\n", "2020030329"));
        list.add(new ItemData(R.drawable.a2020030332,"ELIZALDE VARGAS XIOMARA YAMILETH\n", "2020030332"));
        list.add(new ItemData(R.drawable.a2020030333,"SALCIDO SARABIA JESUS ANTONIO\n", "2020030333"));
        list.add(new ItemData(R.drawable.a2020030389,"RODRIGUEZ SANCHEZ YENNIFER CAROLINA\n", "2020030389"));
        list.add(new ItemData(R.drawable.a2020030766,"FLORES PRADO MANUEL ALEXIS\n", "2020030766"));
        list.add(new ItemData(R.drawable.a2020030771,"AGUIRRE TOSTADO VICTOR MOISES\n", "2020030771"));
        list.add(new ItemData(R.drawable.a2020030777,"DOMINGUEZ SARABIA HALACH UINIC\n", "2020030777"));
        list.add(new ItemData(R.drawable.a2020030799,"MACIEL NUÑEZ ENZO ALEJANDRO\n", "2020030799"));
        list.add(new ItemData(R.drawable.a2020030808,"BARRON VARGAS JOSE ALBERTO\n", "2020030808"));
        list.add(new ItemData(R.drawable.a2020030819,"MARTIN IBARRA GIANCARLO\n", "2020030819"));
        list.add(new ItemData(R.drawable.a2020030865,"SANCHEZ OCEGUEDA LUIS ANGEL\n", "2020030865"));

        lv = (ListView) findViewById(R.id.listView);

        adapter = new List(this, R.layout.activity_list, R.id.lblCategorias, list);

        ListAdapter lista = new List(this,R.layout.activity_list,R.id.lblCategorias,list);
        lv.setAdapter(lista);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                      @Override
                                      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                          Toast.makeText(adapterView.getContext(),getString(R.string.msgSeleccionado).toString() +" "+((ItemData) adapterView.getItemAtPosition(i)).getNombreAlumno(), Toast.LENGTH_SHORT).show();
                                      }

                                  }
        );


    }
    private void resetAdapterData() {
        if (adapter != null) {
            adapter.resetData();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        MenuItem menuItem = menu.findItem(R.id.menusearch);
        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                resetAdapterData();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return true;
            }
        });

        return true;
    }


}
