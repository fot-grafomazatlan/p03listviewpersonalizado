package com.example.listview;

public class ItemData {
    private Integer imgAlumno;
    private String nombreAlumno;
    private String matricula;


    public ItemData(Integer imgAlumno, String nombreAlumno, String matricula) {
        this.imgAlumno = imgAlumno;
        this.nombreAlumno = nombreAlumno;
        this.matricula = matricula;
    }

    //Setter y Getter

    public Integer getImgAlumno() {
        return imgAlumno;
    }

    public void setImgAlumno(Integer imgAlumno) {
        this.imgAlumno = imgAlumno;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
